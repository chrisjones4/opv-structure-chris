# OPV Structure:
### Training 

## Resources:

- freud documentation:  
	- https://freud.readthedocs.io/en/stable/modules/density.html#freud.density.RDF  

- https://github.com/cmelab/notebook_tutorials  
	- Checkout the radial-distribution tutorial

- gsd documentation
	- https://gsd.readthedocs.io/en/stable/



## Prompts:
- Plot the RDF for each temperature using the freud package  
	- Provide some analysis on the plots  
		1. Are there noticeable differences for each temperature?  
		2. What could these differences say about the system at each temperature?  
	- Try plotting multiple RDFs on a single plot with a legend showing the temperatures  


- Do the RDFs look different when using the -centers.gsd files as opposed to the -trajectory.gsd files?  
	- If they are noticeably different what do you think could be the cause?  
	- Do you think it is useful to create RDFs that only look at certain parts of a molecule or system?  

	***note:*** *The files ending in `-centers.gsd` show the trajectory for only the individual molecules rather than every atom of every molecule.  The files ending in `-trajectory.gsd` show the trajectory for every atom in the system.*

- Plot an RDF that only tracks certain atoms.  Possible candidates include fluorine, oxygen and sulfur (See image below) Ignore the carbon atoms for now.  

	***note:*** *To do this you'll have to use the `-trajectories.gsd` files; not the `-centers.gsd` files*  

	***hint:*** *This might require that you use the `gsd` package to see how the atom types are described within the files*  

### PTB7 Monomer
![alt text](https://www.sigmaaldrich.com/content/dam/sigma-aldrich/structure5/121/a_____772410.eps/_jcr_content/renditions/a_____772410-large.png)


